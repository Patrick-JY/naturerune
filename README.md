Simply a Timer that activates when it reads that a chest has been opened, this
is so that respawn timers of the chests can be more easily timed.