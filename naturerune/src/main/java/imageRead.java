import org.bytedeco.javacpp.*;
import org.bytedeco.tesseract.*;
import org.bytedeco.leptonica.*;

import static org.bytedeco.leptonica.global.lept.pixDestroy;
import static org.bytedeco.leptonica.global.lept.pixRead;

public class imageRead {
    public static boolean AnalyseText(String imageLocation){
        String readText = getImgText(imageLocation);
        System.out.println(readText);
        //the screen reader seems to struggle with the a in treasure
        if (readText != null && readText.toLowerCase().contains("open")){
            return true;
        }
        return false;
    }

    public static String getImgText(String imageLocation){
        BytePointer outText;
        TessBaseAPI api = new TessBaseAPI();

        if (api.Init(".","ENG") !=0){
            System.err.println("Could not initialize tesseract");
            System.exit(1);
        }

        PIX image = pixRead(imageLocation);
        api.SetImage(image);

        outText = api.GetUTF8Text();
        String string = outText.getString();
        api.End();
        outText.deallocate();
        pixDestroy(image);
        return string;





    }
}
