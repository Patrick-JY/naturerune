import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SnipIt {
    static JFrame f;
    private Timer Respawntimer;
    static JLabel clock;
    final long respawn = 15000;
    final SimpleDateFormat sdf = new SimpleDateFormat("mm : ss");

    public SnipIt() {

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                }

                JFrame frame = new JFrame();
                frame.setUndecorated(true);
                // This works differently under Java 6
                frame.setBackground(new Color(0, 0, 0, 0));
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setLayout(new BorderLayout());
                frame.add(new SnipItPane());
                frame.setBounds(getVirtualBounds());
                frame.setVisible(true);

                f = new JFrame("timer");

                clock = new JLabel(sdf.format(new Date(respawn)),JLabel.CENTER);
                int x = 0;
                Respawntimer = new Timer(1000,countDown);
                JPanel p = new JPanel();
                p.add(clock);
                f.add(p);
                f.setSize(300,300);
                f.setVisible(true);
            }
        });

    }
    ActionListener countDown = new ActionListener() {
        long x = respawn - 1000;
        @Override
        public void actionPerformed(ActionEvent e) {
            clock.setText(sdf.format(new Date(x)));
            x-=1000;
            if(x <= 0){
                Respawntimer.stop();
                x = respawn - 1000;
                clock.setText(sdf.format(new Date(respawn)));
            }
        }
    };

    public class SnipItPane extends JPanel {

        private Point mouseAnchor;
        private Point dragPoint;

        private SelectionPane selectionPane;

        public SnipItPane() {
            setOpaque(false);
            setLayout(null);
            selectionPane = new SelectionPane();
            add(selectionPane);
            MouseAdapter adapter = new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    mouseAnchor = e.getPoint();
                    dragPoint = null;
                    selectionPane.setLocation(mouseAnchor);
                    selectionPane.setSize(0, 0);
                }

                @Override
                public void mouseDragged(MouseEvent e) {
                    dragPoint = e.getPoint();
                    int width = dragPoint.x - mouseAnchor.x;
                    int height = dragPoint.y - mouseAnchor.y;

                    int x = mouseAnchor.x;
                    int y = mouseAnchor.y;

                    if (width < 0) {
                        x = dragPoint.x;
                        width *= -1;
                    }
                    if (height < 0) {
                        y = dragPoint.y;
                        height *= -1;
                    }
                    selectionPane.setBounds(x, y, width, height);
                    selectionPane.revalidate();
                    repaint();
                }
            };
            addMouseListener(adapter);
            addMouseMotionListener(adapter);
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);

            Graphics2D g2d = (Graphics2D) g.create();

            Rectangle bounds = new Rectangle(0, 0, getWidth(), getHeight());
            Area area = new Area(bounds);
            area.subtract(new Area(selectionPane.getBounds()));

            g2d.setColor(new Color(192, 192, 192, 64));
            g2d.fill(area);

        }
    }

    public class SelectionPane extends JPanel {
        private final static int ONE_SECOND = 1000;
        private final static String NEW_LINE_DLIM = "\n";
        private Timer timer;
        private JButton button;
        private JButton screenshot;
        private JLabel label;

        @Override
        public void paint(Graphics g) {
            super.paint(g);
        }

        public SelectionPane() {

            button = new JButton("Close");
            setOpaque(false);
            screenshot = new JButton("ScreenShot");

            label = new JLabel("Rectangle");
            label.setOpaque(true);
            label.setBorder(new EmptyBorder(4, 4, 4, 4));
            label.setBackground(Color.GRAY);
            label.setForeground(Color.WHITE);

            setLayout(new GridBagLayout());

            GridBagConstraints gbc = new GridBagConstraints();
            gbc.gridx = 0;
            gbc.gridy = 0;
            add(label, gbc);

            gbc.gridy++;
            add(button, gbc);
            gbc.gridy++;
            add(screenshot,gbc);

            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SwingUtilities.getWindowAncestor(SelectionPane.this).dispose();
                }
            });
            screenshot.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(timer.isRunning()){
                        timer.stop();
                    }
                    else {timer.start();}


                    /*Rectangle shotRect = new Rectangle(getX(),getY(),getWidth(),getHeight());

                    try {

                        BufferedImage capture = new Robot().createScreenCapture(shotRect);

                        ImageIO.write(capture,"png",new File("C:\\Users\\Patrick\\IdeaProjects\\naturerune\\naturerune\\src\\main\\resources\\images\\" + "rune.png"));

                    }catch (AWTException | IOException  x){}

                }*/
            }});

            timer = new Timer(ONE_SECOND, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (getWidth() != 0 && getHeight() != 0) {


                        Rectangle shotRect = new Rectangle(getX(), getY(), getWidth(), getHeight());

                        try {

                            BufferedImage capture = new Robot().createScreenCapture(shotRect);

                            ImageIO.write(capture, "png", new File("C:\\Users\\Patrick\\IdeaProjects\\naturerune\\naturerune\\src\\main\\resources\\images\\" + "rune.png"));

                            if (imageRead.AnalyseText("C:\\Users\\Patrick\\IdeaProjects\\naturerune\\naturerune\\src\\main\\resources\\images\\" + "rune.png") && !Respawntimer.isRunning()){
                                Respawntimer.start();
                            }

                        } catch (AWTException | IOException x) {
                        }
                    }
                }
            });

            addComponentListener(new ComponentAdapter() {
                @Override
                public void componentResized(ComponentEvent e) {
                    label.setText("Rectangle " + getX() + "x" + getY() + "x" + getWidth() + "x" + getHeight());

                }
            });

        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g.create();
            // I've chosen NOT to fill this selection rectangle, so that
            // it now appears as if you're "cutting" away the selection
//            g2d.setColor(new Color(128, 128, 128, 64));
//            g2d.fillRect(0, 0, getWidth(), getHeight());

            float dash1[] = {10.0f};
            BasicStroke dashed =
                    new BasicStroke(3.0f,
                            BasicStroke.CAP_BUTT,
                            BasicStroke.JOIN_MITER,
                            10.0f, dash1, 0.0f);
            g2d.setColor(Color.BLACK);
            g2d.setStroke(dashed);
            g2d.drawRect(0, 0, getWidth() - 3, getHeight() - 3);
            g2d.dispose();
        }
    }

    public static Rectangle getVirtualBounds() {

        Rectangle bounds = new Rectangle(0, 0, 0, 0);

        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice lstGDs[] = ge.getScreenDevices();
        for (GraphicsDevice gd : lstGDs) {

            bounds.add(gd.getDefaultConfiguration().getBounds());

        }

        return bounds;

    }
}